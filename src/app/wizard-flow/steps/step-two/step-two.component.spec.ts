import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCard } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { StepTwoComponent } from './step-two.component';
import { StepTwoService } from './step-two.service';
import { AngularMaterialModule } from '../../../angular-material/angular-material.module';

describe('StepTwoComponent', () => {
  let component: StepTwoComponent;
  let fixture: ComponentFixture<StepTwoComponent>;

  const mockStepTwoService = jasmine.createSpyObj('StepTwoService', ['getStepTwo']);
  mockStepTwoService.getStepTwo.and.returnValue({ address: 'Jiron Colina 1263', mobile: '999381764' });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[
        AngularMaterialModule,
        ReactiveFormsModule,
        BrowserAnimationsModule
      ],
      declarations: [ StepTwoComponent ],
      providers:[
        {provide: StepTwoService, useValue: mockStepTwoService}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return Jiron Colina 1263 as address and 999381764 as mobile', () => {
    const updateForm = mockStepTwoService.getStepTwo();
    expect(updateForm.address).toBe('Jiron Colina 1263');
    expect(updateForm.mobile).toBe('999381764');
  });
});
