import { Injectable } from '@angular/core';

import { StepTwoModel } from './step-two.model';

@Injectable()
export class StepTwoService {

  private stepTwoForm: StepTwoModel = new StepTwoModel();
  
  constructor() { }

  getStepTwo(){
    let form = {
      address: this.stepTwoForm.address,
      mobile: this.stepTwoForm.mobile
    }
    return form;
  }

  setStepTwo(data: StepTwoModel){
    this.stepTwoForm.address = data.address;
    this.stepTwoForm.mobile = data.mobile;
  }

}