import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatCard } from '@angular/material';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/filter';

import { StepTwoService } from './step-two.service';

@Component({
  selector: 'app-step-two',
  templateUrl: './step-two.component.html',
  styleUrls: ['./step-two.component.css']
})
export class StepTwoComponent implements OnInit {

  stepTwoForm: FormGroup;
  currentFormStatus: Subject<boolean> = new Subject();
  formAttributes: Subject<any> = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    private stepService: StepTwoService,
  ) { 
    this.createStepTwoForm();
  }

  ngOnInit() {
    this.stepTwoForm.setValue(this.stepService.getStepTwo());
    this.subcribeToFormStatusChanges();
    this.currentFormStatus.next(this.getFormStatus());
  }

  subcribeToFormStatusChanges() {
      // initialize stream
      let myFormValueChanges$ = this.stepTwoForm.statusChanges
      .filter(value => value === 'VALID' || value === 'INVALID');

      // subscribe to the stream 
      myFormValueChanges$.subscribe(value => {
        if (value === 'VALID'){
          this.currentFormStatus.next(this.getFormStatus());
          this.formAttributes.next(this.stepTwoForm.getRawValue());
          this.stepService.setStepTwo(this.stepTwoForm.getRawValue());
          return;
        }
        this.currentFormStatus.next(this.getFormStatus());
      });
  }

  getFormStatus(){
    return this.stepTwoForm.status === 'VALID' ? true : false;
  }

  createStepTwoForm(){
    this.stepTwoForm = this.formBuilder.group({
      address: ['', Validators.required],
      mobile: ['', Validators.required]
    })
  }

  get address() { return this.stepTwoForm.get('address')};
  get mobile() { return this.stepTwoForm.get('mobile')};

}
