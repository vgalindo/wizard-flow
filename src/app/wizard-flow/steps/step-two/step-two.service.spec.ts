import { TestBed, inject } from '@angular/core/testing';

import { StepTwoService } from './step-two.service';

describe('StepTwoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StepTwoService]
    });
  });

  it('should be created', inject([StepTwoService], (service: StepTwoService) => {
    expect(service).toBeTruthy();
  }));
});
