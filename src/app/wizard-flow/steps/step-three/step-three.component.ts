import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatCard } from '@angular/material';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/filter';

import { StepThreeService } from './step-three.service';

@Component({
  selector: 'app-step-three',
  templateUrl: './step-three.component.html',
  styleUrls: ['./step-three.component.css']
})
export class StepThreeComponent implements OnInit {

  stepThreeForm: FormGroup;
  currentFormStatus: Subject<boolean> = new Subject();
  formAttributes: Subject<any> = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    private stepService: StepThreeService,
  ) { 
    this.createStepThreeForm();
  }

  ngOnInit() {
    this.stepThreeForm.setValue(this.stepService.getStepThree());
    this.subcribeToFormStatusChanges();
    this.currentFormStatus.next(this.getFormStatus());
  }

  subcribeToFormStatusChanges() {
      // initialize stream
      let myFormValueChanges$ = this.stepThreeForm.statusChanges
      .filter(value => value === 'VALID' || value === 'INVALID');

      // subscribe to the stream 
      myFormValueChanges$.subscribe(value => {
        if (value === 'VALID'){
          this.currentFormStatus.next(this.getFormStatus());
          this.formAttributes.next(this.stepThreeForm.getRawValue());
          this.stepService.setStepThree(this.stepThreeForm.getRawValue());
          return;
        }
        this.currentFormStatus.next(this.getFormStatus());
      });
  }

  getFormStatus(){
    return this.stepThreeForm.status === 'VALID' ? true : false;
  }

  createStepThreeForm(){
    this.stepThreeForm = this.formBuilder.group({
      notes: ['', Validators.required],
    })
  }

  get notes() { return this.stepThreeForm.get('notes')};

}
