import { Injectable } from '@angular/core';

import { StepThreeModel } from './step-three.model';

@Injectable()
export class StepThreeService {

  private stepThreeForm: StepThreeModel = new StepThreeModel();
  
  constructor() { }

  getStepThree(){
    let form = {
      notes: this.stepThreeForm.notes,
    }
    return form;
  }

  setStepThree(data: StepThreeModel){
    this.stepThreeForm.notes = data.notes;
  }

}