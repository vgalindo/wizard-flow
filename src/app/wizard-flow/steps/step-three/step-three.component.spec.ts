import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCard } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { StepThreeComponent } from './step-three.component';
import { StepThreeService } from './step-three.service';
import { AngularMaterialModule } from '../../../angular-material/angular-material.module';

describe('StepThreeComponent', () => {
  let component: StepThreeComponent;
  let fixture: ComponentFixture<StepThreeComponent>;

  const mockStepThreeService = jasmine.createSpyObj('StepThreeService', ['getStepThree']);
  mockStepThreeService.getStepThree.and.returnValue({notes: 'note123'});

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[
        AngularMaterialModule,
        ReactiveFormsModule,
        BrowserAnimationsModule
      ],
      declarations: [ StepThreeComponent ],
      providers:[
        {provide: StepThreeService, useValue: mockStepThreeService}
        
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return note123 as note', () => {
    const updateForm = mockStepThreeService.getStepThree();
    expect(updateForm.notes).toBe('note123');
  });
});
