import { TestBed, inject } from '@angular/core/testing';

import { StepThreeService } from './step-three.service';

describe('StepThreeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StepThreeService]
    });
  });

  it('should be created', inject([StepThreeService], (service: StepThreeService) => {
    expect(service).toBeTruthy();
  }));
});
