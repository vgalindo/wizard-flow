import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatCard } from '@angular/material';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/filter';

import { StepOneService } from './step-one.service';
import { StepOneModel } from './step-one.model';
@Component({
  selector: 'app-step-one',
  templateUrl: './step-one.component.html',
  styleUrls: ['./step-one.component.css']
})
export class StepOneComponent implements OnInit {

  stepOneForm: FormGroup;
  currentFormStatus: Subject<boolean> = new Subject();
  formAttributes: Subject<StepOneModel> = new Subject();

  constructor(
    private formBuilder: FormBuilder,
    private stepService: StepOneService,
  ) { 
    this.createStepOneForm();
  }

  ngOnInit() {
    this.stepOneForm.setValue(this.stepService.getStepOne());
    this.subcribeToFormStatusChanges();
    this.currentFormStatus.next(this.getFormStatus());
  }

  subcribeToFormStatusChanges() {
      // initialize stream
      let myFormValueChanges$ = this.stepOneForm.statusChanges
      .filter(value => value === 'VALID' || value === 'INVALID');

      // subscribe to the stream 
      myFormValueChanges$.subscribe(value => {
        if (value === 'VALID'){
          this.currentFormStatus.next(this.getFormStatus());
          this.formAttributes.next(this.stepOneForm.getRawValue());
          this.stepService.setStepOne(this.stepOneForm.getRawValue());
          return;
        }
        this.currentFormStatus.next(this.getFormStatus());
      });
  }

  getFormStatus(){
    return this.stepOneForm.status === 'VALID' ? true : false;
  }

  createStepOneForm(){
    this.stepOneForm = this.formBuilder.group({
      name: ['', Validators.required],
      last_name: ['', Validators.required]
    })
  }

  get name() { return this.stepOneForm.get('name')};
  get last_name() { return this.stepOneForm.get('last_name')};
}
