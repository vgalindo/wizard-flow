import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCard } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { StepOneComponent } from './step-one.component';
import { StepOneService } from './step-one.service';
import { AngularMaterialModule } from '../../../angular-material/angular-material.module';

describe('StepOneComponent', () => {
  let component: StepOneComponent;
  let fixture: ComponentFixture<StepOneComponent>;

  const mockStepOneService = jasmine.createSpyObj('StepOneService', ['getStepOne']);
  mockStepOneService.getStepOne.and.returnValue({ name: 'Vlad', last_name: 'Galindo' });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[
        AngularMaterialModule,
        ReactiveFormsModule,
        BrowserAnimationsModule
      ],
      declarations: [ StepOneComponent ],
      providers:[
        {provide: StepOneService, useValue: mockStepOneService}
        /* {provide: StepOneService, useClass: StepOneServiceStub} */
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
  it('should return Vlad as name and Galindo as last_name', () => {
    const updateForm = mockStepOneService.getStepOne();
    expect(updateForm.name).toBe('Vlad');
    expect(updateForm.last_name).toBe('Galindo');
  });
});
