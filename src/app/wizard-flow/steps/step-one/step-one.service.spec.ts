import { TestBed, inject } from '@angular/core/testing';

import { StepOneService } from './step-one.service';

describe('StepOneService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StepOneService]
    });
  });

  it('should be created', inject([StepOneService], (service: StepOneService) => {
    expect(service).toBeTruthy();
  }));
});
