import { Injectable } from '@angular/core';

import { StepOneModel } from './step-one.model';

@Injectable()
export class StepOneService {

  private stepOneForm: StepOneModel = new StepOneModel();
  
  constructor() { }

  getStepOne(){
    let form = {
      name: this.stepOneForm.name,
      last_name: this.stepOneForm.last_name
    }
    return form;
  }

  setStepOne(data: StepOneModel){
    this.stepOneForm.name = data.name;
    this.stepOneForm.last_name = data.last_name;
  }

}
