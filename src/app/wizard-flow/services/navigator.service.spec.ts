import { TestBed, inject } from '@angular/core/testing';

import { NavigatorService } from './navigator.service';
import { Step } from '../models/steps';
import { STEPS } from '../mocks/steps-refactor';

describe('NavigatorService', () => {
  let mockSTEPS: Step[] = STEPS;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NavigatorService]
    });
    let mockSTEPS: Step[] = STEPS;
    let mockLength = mockSTEPS.length;
  });

  it('should be created', inject([NavigatorService], (service: NavigatorService) => {
    expect(service).toBeTruthy();
  }));

  it('Return an Array of STEPS', inject([NavigatorService], (service: NavigatorService)=>{
    let compareSTEPS = service.getSteps();

    expect(mockSTEPS).toEqual(compareSTEPS);
  }));
  
  it('Return an Step from the Array of STEPS', inject([NavigatorService], (service: NavigatorService)=>{
    let position = {new: 1, old: 0};

    let compareSTEPS = service.recoverStep(position);

    expect(mockSTEPS[1]).toEqual(compareSTEPS);
  }));

  it('Return the length of the Array', inject([NavigatorService], (service: NavigatorService)=>{
    let stepsLenght = mockSTEPS.length;
    
    let length = service.getSteps().length;

    expect(length).toEqual(stepsLenght);
  }));


  it('Set Current a new position of the Array of Steps', inject([NavigatorService], (service: NavigatorService)=>{
    let newPosition = 1;
    
    service.setCurrent(newPosition);

    expect(STEPS[1].data.isCurrent).toBeTruthy();
  }));

  it('Moving Foward on the Array of Steps', inject([NavigatorService], (service: NavigatorService)=>{
    let newPosition = 1;
    let spy = spyOn(service, 'setCurrent').and.callFake(()=>{});
    
    service.goFoward(newPosition);

    expect(spy).toHaveBeenCalled();
  }));

  it('Setter and Getter for private position property', inject([NavigatorService], (service: NavigatorService)=>{
    let value = 1;
    service.setPosition(value);
    
    let chkValue = service.getPosition();

    expect(chkValue).toEqual(value);
  }));

  it('Setter and Getter for private oldPosition property', inject([NavigatorService], (service: NavigatorService)=>{
    let value = 1;
    service.setOldPosition(value);
    
    let chkValue = service.getOldPosition();

    expect(chkValue).toEqual(value);
  }));
  
  it('Setter and Getter for private finalForm property', inject([NavigatorService], (service: NavigatorService)=>{
    let value = {name: 'test'};
    service.formData(value);
    
    let chkValue = service.getFormData();

    expect(chkValue).toEqual(value);
  }));

});
