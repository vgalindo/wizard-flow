import { Injectable, Type } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { STEPS } from '../mocks/steps-refactor';
import { Step } from '../models/steps';
import { StepOneModel } from '../steps/step-one/step-one.model';

@Injectable()
export class NavigatorService {

  constructor(
    
  ) { }

  private position: number;
  private oldPosition: number;
  private currentFormStatus: boolean;
  private finalForm: any = {};
  private stepOneForm: StepOneModel = new StepOneModel();
  
  getSteps(): Array<Step>{
    return STEPS;
  }

  recoverStep(position: any): Step{
    return STEPS[position.new];
  }

  stepsLength(){
    return this.getSteps().length;
  }

  setCurrent(value: number){
    STEPS.forEach((item, index)=>{
      item.data.isCurrent = index === value ? true : false;
    })
    this.oldPosition = this.position ? this.position : 0 ; 
    this.position = value;
    //console.log(`position: ${this.position}, oldPosition ${this.oldPosition}`);
  }

  goFoward(position = this.position){
    let length = this.stepsLength();
    if(position < length - 1){
      //let position = this.position;
      let newPosition = ++position;
      this.setCurrent(newPosition);
    }
  }

  goBackward(position = this.position){
    //let length = this.stepsLength();
    if(position > 0){
      //let position = this.position;
      let newPosition = --position;
      this.setCurrent(newPosition);
    }
  }

  getPosition(): number{
    return this.position;
  }

  setPosition(value: number){
    this.position = value;
  }
  
  getOldPosition(): number{
    return this.oldPosition;
  }

  setOldPosition(value: number){
    this.oldPosition = value;
  }

  formData(data: object){
    for (let [key, value] of Object.entries(data)) {
        this.finalForm[key] = value;
    }
  }

  getFormData(){
    return this.finalForm;
  }
  
}
