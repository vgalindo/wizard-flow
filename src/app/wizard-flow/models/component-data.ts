export interface ComponentData{
    name: string;
    title: string;
    subtitle: string;
    isValid: boolean;
    isCurrent: boolean;
    label: string;
}