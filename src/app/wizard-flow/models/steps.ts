import { Type } from '@angular/core';
import { ComponentData } from './component-data';

export class Step {
  constructor(public component: Type<any>, public data: ComponentData) {}
}
