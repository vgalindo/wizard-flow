import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NgModule, Directive, Component, Input, Output, EventEmitter } from '@angular/core';
import { MatCardModule } from '@angular/material';
import { By } from '@angular/platform-browser';

import { WizardFlowComponent } from './wizard-flow.component';
/* import { StepIndicatorComponent } from './step-indicator/step-indicator.component';
import { NavigatorComponent } from './navigator/navigator.component'; */
import { WizardStepsDirective } from './directives/wizard-steps.directive';
import { NavigatorService } from './services/navigator.service';
import { AngularMaterialModule } from '../angular-material/angular-material.module';
/* import { StepOneComponent } from './steps/step-one/step-one.component'; */
import { Step } from './models/steps';
import { Subject } from 'rxjs/Subject';
import { StepOneModel } from './steps/step-one/step-one.model';

@Component({
  selector: 'app-step-one',
  template: '',
  styles: ['']
})
class StepOneComponent{
  currentFormStatus: Subject<boolean> = new Subject();
  formAttributes: Subject<StepOneModel> = new Subject();
}

@Component({
  selector: 'app-step-two',
  template: '',
  styles: ['']
})
class StepTwoComponent{
  currentFormStatus: Subject<boolean> = new Subject();
  formAttributes: Subject<StepOneModel> = new Subject();
}

@Directive({
  selector: 'app-navigator',
})
class mockNavigatorComponent{
  @Input() childCurrentFormValid: boolean = true;
  @Output() childNewPosition = new EventEmitter<{new: number, old?: number}>();
}

@Directive({
  selector: 'step-indicator',
})
class mockStepIndicatorComponent{
  @Input() childIndicator: any;
}

describe('WizardFlowComponent', () => {
  let component: WizardFlowComponent;
  let fixture: ComponentFixture<WizardFlowComponent>;

  class mockNavigatorService extends NavigatorService{
    recoverStep(position: any): Step{
      return {component: StepOneComponent, data: {name: 'StepOneComponent', title: 'Step One', subtitle: 'This is the step one', isValid: false, isCurrent: false, label: 'This is the step one'}};
    }
    formData(data: object){
    }
    getFormData(){
      return {name:''};
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[
        AngularMaterialModule,
        ReactiveFormsModule,
        MatCardModule
      ],
      declarations: [ 
        WizardStepsDirective,
        WizardFlowComponent,
        mockStepIndicatorComponent,
        mockNavigatorComponent,
       ],
       providers:[
        { provide: NavigatorService, useClass: mockNavigatorService }
       ],
    }).overrideModule(BrowserDynamicTestingModule, {
        set: {
          declarations: [ 
            StepOneComponent,
            StepTwoComponent
          ],
          entryComponents: [
            StepOneComponent,
            StepTwoComponent
          ],
        },
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WizardFlowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /* it('should load a Component instance on the ng-template once the ng-template is called', inject([NavigatorService],(service: NavigatorService)=>{
    component.ngOnInit;
    fixture.detectChanges();

    let de = fixture.debugElement.query(By.css('.wizard-steps'));
    console.log('de ' + de);

    let step: HTMLElement = de.nativeElement;

    console.log('step' + step);
    //let button: HTMLElement = de.nativeElement;

    expect(de.componentInstance).toEqual(StepOneComponent);
  })); */
});
