import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { AngularMaterialModule } from '../../angular-material/angular-material.module';
import { NavigatorComponent } from './navigator.component';
import { NavigatorService } from '../services/navigator.service';
import { By } from '@angular/platform-browser';

describe('NavigatorComponent', () => {
  let component: NavigatorComponent;
  let fixture: ComponentFixture<NavigatorComponent>;

  const mockNavigatorService: NavigatorService = jasmine.createSpyObj<NavigatorService>('NavigatorService', ['setCurrent', 'stepsLength', 'goFoward', 'getPosition', 'getOldPosition', 'goBackward']);
  

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[AngularMaterialModule],
      declarations: [ 
        NavigatorComponent,
       ],
      providers:[
        {provide: NavigatorService, useValue: mockNavigatorService },
        
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigatorComponent);
    component = fixture.componentInstance;

    let mockPosition: number;

    (<jasmine.Spy>mockNavigatorService.setCurrent).and.callFake((val)=>{
      mockPosition = val;
    });
    (<jasmine.Spy>mockNavigatorService.stepsLength).and.returnValue(3);
    (<jasmine.Spy>mockNavigatorService.goFoward).and.callFake(()=>{});
    (<jasmine.Spy>mockNavigatorService.getPosition).and.callFake(()=>{ return mockPosition; });
    (<jasmine.Spy>mockNavigatorService.getOldPosition).and.returnValue(0);
    (<jasmine.Spy>mockNavigatorService.goBackward).and.callFake(()=>{});

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set steps length on ngOnIbit call', inject([NavigatorService], (service: NavigatorService)=>{
    
    component.ngOnInit();

    expect(component.stepsLength).toEqual(3);
  }));

  it('should check if the foward button is available on the view if Form is Valid and not in the last step', ()=>{
    component.childCurrentFormValid = true;
    /* Important step to make change on the template */
    fixture.detectChanges();
    let de = fixture.debugElement.query(By.css('.FowardButton'));
    let button: HTMLElement = de.nativeElement;
    let disabled = button.getAttribute('ng-reflect-disabled');
    expect(disabled).toBe('false');
  });

  it('should call the goFoward when clicking the foward button on the view', ()=>{
    let spy = spyOn(component, 'goFoward').and.callFake(()=>{});
    let de = fixture.debugElement.query(By.css('.FowardButton'));
    let button: HTMLElement = de.nativeElement;
    button.click();
    expect(spy).toHaveBeenCalled();
  });

  it('should call Emit a childNewPosition when goFoward f(x) gets called', inject([NavigatorService], (service: NavigatorService)=>{
    service.setCurrent(1);
    let newPosition: {new: number, old: number};
    fixture.detectChanges();
    component.childNewPosition.subscribe(val => newPosition = val);
    component.goFoward();
    console.log(newPosition);
    expect(service.goFoward).toHaveBeenCalled();
    expect(newPosition).toEqual({new: 1, old: 0});
  }));

  it('should check if the the Finish button is available on the view if Form is Valid and IS the last step', inject([NavigatorService], (service: NavigatorService)=>{
    
    component.childCurrentFormValid = true;
    service.setCurrent(2);
    /* Important step to make change on the template */
    fixture.detectChanges();
    component.checkFowardButton();
    let de = fixture.debugElement.query(By.css('.FinishButton'));
    let button: HTMLElement = de.nativeElement;
    let disabled = button.getAttribute('ng-reflect-disabled');
    expect(disabled).toBe('false');

  }));

  it('should check if the the Cancel button is available on the view if IS the first step', inject([NavigatorService], (service: NavigatorService)=>{
    
    service.setCurrent(0);
    component.checkInitialStep();
    /* Important step to make change on the template */
    fixture.detectChanges();
    let de = fixture.debugElement.query(By.css('.CancelButton'));
    let button: HTMLElement = de.nativeElement;
    expect(button).not.toBeNull();

  }));

  it('should check if the back button is available on the view if not in the first step', inject([NavigatorService], (service: NavigatorService)=>{
    service.setCurrent(1);
    component.checkBackButton();
    /* Important step to make change on the template */
    fixture.detectChanges();
    let de = fixture.debugElement.query(By.css('.BackButton'));
    let button: HTMLElement = de.nativeElement;
    let disabled = button.getAttribute('ng-reflect-disabled');
    expect(disabled).toBe('false');
  }));

});
