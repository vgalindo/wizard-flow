import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material';

import { NavigatorService } from '../services/navigator.service';
import { UniversalConfirmationDialogComponent } from '../../universal-confirmation-dialog/universal-confirmation-dialog.component';
@Component({
  selector: 'app-navigator',
  templateUrl: './navigator.component.html',
  styleUrls: ['./navigator.component.css']
})
export class NavigatorComponent implements OnInit {

  /* @Input() */ stepsLength: number;
  /* @Input() */ position: number = 0;
  @Input() childCurrentFormValid: boolean = true;
  @Output() childNewPosition = new EventEmitter<{new: number, old?: number}>();
  // private oldPosition: number;
  
  constructor(
    private navigatorServ: NavigatorService,
    private dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.navigatorServ.setCurrent(0);
    this.stepsLength = this.navigatorServ.stepsLength();
  }

  goFoward(){
    this.navigatorServ.goFoward();
    let newP = this.navigatorServ.getPosition();
    let oldPosition = this.navigatorServ.getOldPosition();
    // this.childCurrentFormValid = true;
    this.childNewPosition.emit({new: newP, old: oldPosition});
    //console.log(newP);
  }

  checkFormValidation(){
    if(!this.childCurrentFormValid){
      return true;
    }
    return false;
  }

  checkFowardButton(){
    return this.checkFormValidation() === true ? true : this.navigatorServ.getPosition() >= this.navigatorServ.stepsLength() - 1;
  }

  goBack(){
    this.navigatorServ.goBackward();
    let newP = this.navigatorServ.getPosition();
    let oldPosition = this.navigatorServ.getOldPosition();
    this.childNewPosition.emit({new: newP, old: oldPosition});
    //console.log(newP);
  }

  checkBackButton(){
    return this.navigatorServ.getPosition() <= 0 ? true : false;
  }

  checkFinalStep(){
    return this.navigatorServ.getPosition() === this.navigatorServ.stepsLength() - 1 ? true : false;
  }
  
  checkInitialStep(){
    return this.navigatorServ.getPosition() === 0 ? true : false;
  }

  cancelWizard(){
    let cancelDialog = this.dialog.open(UniversalConfirmationDialogComponent,{
      width: '500px',
      data: {
        title: 'Wizard',
        body: 'Wizard cancelado',
        buttons : {
          ok: 'Ok',
          cancel: 'Cancel'
        },
        info: false,
        icon: 'check'
      }
    });

    cancelDialog.afterClosed().subscribe(res => {
      console.log(res);
      if(res === true){
        //this.updateLanguage(this.languageForm.value);
      }
    })
  };
  
  finishWizard(){
    let finishDialog = this.dialog.open(UniversalConfirmationDialogComponent,{
      width: '500px',
      data: {
        title: 'Wizard',
        body: 'Wizard Terminado / datos enviados',
        buttons : {
          ok: 'Ok',
          cancel: 'Cancel'
        },
        info: false,
        icon: 'check'
      }
    });

    finishDialog.afterClosed().subscribe(res => {
      console.log(res);
      if(res === true){
        //this.updateLanguage(this.languageForm.value);
      }
    })
  };

}
