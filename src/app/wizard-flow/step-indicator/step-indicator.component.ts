import { Component, OnInit, Input, OnChanges } from '@angular/core';

import { NavigatorService } from '../services/navigator.service';
@Component({
  selector: 'step-indicator',
  templateUrl: './step-indicator.component.html',
  styleUrls: ['./step-indicator.component.scss']
})
export class StepIndicatorComponent implements OnInit, OnChanges {

  @Input() childIndicator: any;
  stepsCollection: any;
  stepName: string;

  constructor(
    private navigationServ: NavigatorService
  ) { }

  ngOnInit() {
    this.stepsCollection = this.navigationServ.getSteps()
  }

  ngOnChanges(){
    //console.log(this.childIndicator);
    this.stepIndicator(this.childIndicator);
  }

  stepIndicator(value){
    //console.log(value);
    this.stepName = value.name;
  }
}
