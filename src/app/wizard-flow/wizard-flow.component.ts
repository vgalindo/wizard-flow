import { Input, ViewChild, Component, OnInit, AfterViewInit, OnDestroy, ComponentFactoryResolver, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';

import { WizardStepsDirective } from './directives/wizard-steps.directive';
import { NavigatorService } from './services/navigator.service';
import { Step } from './models/steps';
import { ComponentData } from './models/component-data';

@Component({
  selector: 'app-wizard-flow',
  templateUrl: './wizard-flow.component.html',
  styleUrls: ['./wizard-flow.component.css']
})
export class WizardFlowComponent implements OnInit, OnDestroy {

  public currentFormValid: boolean;
  /* envia un objeto con información del paso actual al StepIndicatorComponent */
  public stepIndicator: ComponentData;
  allFormsData: any;
  /* Decora el WizardStepsDirective con el @ViewChild exponiendo methods para crear componentes*/
  @ViewChild(WizardStepsDirective) wizardSteps: WizardStepsDirective;

  /* referencia del componente creado */
  private componentRef: any;

  constructor(
    private navigatorServ: NavigatorService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit(){
    //this.steps = this.navigatorServ.getSteps();
    this.newComponent({new : 0});
  }

  ngOnDestroy() {
    console.log('ngOnDestroy');
    this.componentRef.destroy();
   }

  /* Funcion que recibe una posición de array y carga los componentes en el elemento con la directriz  WizardStepsDirective*/
  loadStep(component){
    /* se obtiene la referencia del componente a crear con el method resolveComponentFactory */
    let componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);
    /* se obtiene la referencia del contenedor para el componente */
    let viewContainerRef = this.wizardSteps.viewContainerRef;
    /* se limpia el container */
    viewContainerRef.clear();
    /* se crea el componente en el contenedor */
    this.componentRef = viewContainerRef.createComponent(componentFactory);

    (this.componentRef.instance.currentFormStatus).subscribe(value => {
      this.currentFormValid = value;
    });

    (this.componentRef.instance.formAttributes).subscribe(stepData => {
      this.navigatorServ.formData(stepData);
      this.allFormsData = this.navigatorServ.getFormData();
    });

    /* detects changes -- after every round of change detection, dev mode immediately performs a second round to verify that no bindings have changed since the end of the first, as this would indicate that changes are being caused by change detection itself */
    this.cdr.detectChanges();
  }

  newComponent(positions: any){
    /* var let step es igualada con posición del array de componentes */
    let step = this.navigatorServ.recoverStep(positions);
    /* this.stepIndicator esta enlazada con el StepIndicatorComponente para compartir información */
    this.stepIndicator = step.data;
    /* llama a la funcion loadStep con el componente como parametro */
    this.loadStep(step.component);
  }

}
