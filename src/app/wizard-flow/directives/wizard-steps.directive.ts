import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[wizard-steps]'
})
export class WizardStepsDirective {

  constructor(
    public viewContainerRef: ViewContainerRef
  ) { }

}
