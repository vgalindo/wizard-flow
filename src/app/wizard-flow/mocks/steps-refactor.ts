import { Injectable } from '@angular/core';

import { StepOneComponent } from '../steps/step-one/step-one.component';
import { StepTwoComponent } from '../steps/step-two/step-two.component';
import { StepThreeComponent } from '../steps/step-three/step-three.component';
import { Step } from '../models/steps';


export let STEPS: Step[] = [
      new Step(StepOneComponent, {name: 'StepOneComponent', title: 'Step One', subtitle: 'This is the step one', isValid: false, isCurrent: false, label: 'This is the step one'}),
      new Step(StepTwoComponent, {name: 'StepTwoComponent', title: 'Step Two', subtitle: 'This is the step two', isValid: false, isCurrent: false, label: 'This is the step two'}),
      new Step(StepThreeComponent, {name: 'StepThreeComponent', title: 'Step Three', subtitle: 'This is the step three', isValid: false, isCurrent: false, label: 'This is the step three'}),
    ];
 
