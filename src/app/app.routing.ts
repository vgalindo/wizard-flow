import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { WizardFlowComponent } from './wizard-flow/wizard-flow.component';
import { KabbageComponent } from './kabbage/kabbage.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: '/wizard',
        pathMatch: 'full'
    },
    {
        path: 'kabbage',
        component: KabbageComponent
    },
    {
        path: 'wizard',
        component: WizardFlowComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}