import { Component, /* OnInit */ } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import 'rxjs/operator/map';

import { NavigatorService } from './wizard-flow/services/navigator.service';
import { Step } from './wizard-flow/models/steps';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ NavigatorService ],
})
export class AppComponent {
  title = 'wizard';
  steps: any;

  constructor(
    private http: HttpClient,
    private navigatorServ: NavigatorService,
    private router: Router
  ){}

  /* ngOnInit(){
    //this.steps = this.navigatorServ.getSteps();
  } */

  /* test(e){
    console.log(e)
  } */
  goTo(link:string){
    switch(link) { 
      case "wizard": { 
        this.router.navigate(['wizard/']);
        console.log("Excellent"); 
        break; 
      } 
      case "kabbage": { 
        this.router.navigate(['kabbage/']);
        console.log("Good"); 
        break; 
      }
      default: { 
        this.router.navigate(['wizard/']);
        console.log("Invalid choice"); 
        break;              
      } 
   }
  }

}
