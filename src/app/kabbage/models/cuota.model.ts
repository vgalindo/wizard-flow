export interface CuotaData{
    "saldoCapital": number,
    "numeroCuota": number,
    "amortizacion": number,
    "seguro": number,
    "comision": number,
    "montoCuota": number
}