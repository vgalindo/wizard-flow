export interface KabbageData{
    "desembolso": number,
    "nroCuotas": number,
    "primeraComision": number,
    "ultimaComision": number,
}