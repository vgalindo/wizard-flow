import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AngularMaterialModule } from '../angular-material/angular-material.module';
import { KabbageComponent } from './kabbage.component';
import { KabbageService } from './services/kabbage.service';

describe('KabbageComponent', () => {
  let component: KabbageComponent;
  let fixture: ComponentFixture<KabbageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[
        BrowserAnimationsModule,
        AngularMaterialModule,
        HttpClientModule, 
        ReactiveFormsModule
      ],
      declarations: [ KabbageComponent ],
      providers: [KabbageService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KabbageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
