import { Component, OnInit, } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatCard, MatButton } from '@angular/material';
import { Subject } from 'rxjs/Subject';

import { KabbageData } from './models/kabbage.model';
import { CuotaData } from './models/cuota.model';
import { KabbageService } from './services/kabbage.service';
@Component({
  selector: 'app-kabbage',
  templateUrl: './kabbage.component.html',
  styleUrls: ['./kabbage.component.scss']
})
export class KabbageComponent implements OnInit {

  kabbageForm: FormGroup;
  cuotas: any;
  sliderValue;
  quantityCuotas: number[] = [1,2,3,4,5,6];
  selectedValue;
  firstComission:number = 1;
  lastComission:number = 6;

  constructor(
    private http: HttpClient,
    private kabbageService: KabbageService,
    private formBuilder: FormBuilder
  ) { this.createKabbageForm(); }

  ngOnInit() {
  }

  getCuotas(){
    let parametros = this.kabbageForm.getRawValue();
    this.kabbageService.recalculoCuota(parametros).subscribe(
      data => { this.cuotas = data }
    );
    //console.log( this.cuotas.body );
  }

  createKabbageForm(){
    this.kabbageForm = this.formBuilder.group({
      desembolso: ['', Validators.required],
      nroCuotas: ['', Validators.required],
      primeraComision: ['', Validators.required],
      ultimaComision: ['', Validators.required]
    })
  }

  get desembolso() { return this.kabbageForm.get('desembolso') };
  get nroCuotas() { return this.kabbageForm.get('nroCuotas') };
  get primeraComision() { return this.kabbageForm.get('primeraComision') };
  get ultimaComision() { return this.kabbageForm.get('ultimaComision') };

}
