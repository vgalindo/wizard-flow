import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { KabbageService } from './kabbage.service';

describe('KabbageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientModule ],
      providers: [KabbageService]
    });
  });

  it('should be created', inject([KabbageService], (service: KabbageService) => {
    expect(service).toBeTruthy();
  }));
});
