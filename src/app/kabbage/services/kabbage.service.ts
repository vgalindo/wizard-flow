import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { CuotaData } from '../models/cuota.model';

import { KabbageData } from '../models/kabbage.model';
@Injectable()
export class KabbageService {

  constructor(
    private http: HttpClient
  ) { }

  private url = 'http://192.168.8.104:8080/schedule';

  recalculoCuota(parametros: KabbageData){
    
    let header = new HttpHeaders();
    header = header.append('Authorization', 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1ZCI6IndlYiIsImV4cCI6MTUxMDI2NDAyMCwiaWF0IjoxNTA5NjU5MjIwfQ.fxIDUae9M0XyDLdUhJVqm4c-EOKdAXHwPE4o-7Somon2dWjFJ8pT5AqUfRjFH7qm6RNOte8pi9De6w2KoN6pHQ');
    header = header.append('Content-Type', 'application/json');
    return this.http.post(this.url, parametros, {observe: 'response', headers: header});
  }
}
