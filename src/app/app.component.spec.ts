import { TestBed, async } from '@angular/core/testing';
import { Directive } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';

import { AppComponent } from './app.component';
import { WizardFlowComponent } from './wizard-flow/wizard-flow.component';
import { AngularMaterialModule } from './angular-material/angular-material.module';
describe('AppComponent', () => {

  @Directive({
    selector: 'app-wizard-flow'
  })
  class mockWizardFlowComponent{}

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[
        AngularMaterialModule,
        HttpClientModule,
        RouterTestingModule
      ],
      declarations: [
        AppComponent,
        mockWizardFlowComponent
      ],
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'wizard'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('wizard');
  }));
  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('wizard');
  }));
});
