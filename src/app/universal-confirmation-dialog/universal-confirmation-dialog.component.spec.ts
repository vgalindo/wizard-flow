import { NgModule } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AngularMaterialModule } from '../angular-material/angular-material.module';
import { MatDialog, MatDialogModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UniversalConfirmationDialogComponent } from './universal-confirmation-dialog.component';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  imports:[BrowserModule, AngularMaterialModule,],
  declarations:[UniversalConfirmationDialogComponent],
  entryComponents: [UniversalConfirmationDialogComponent],
  exports: [UniversalConfirmationDialogComponent],
})
class UniversalConfirmationDialogModule {}

describe('UniversalConfirmationDialogComponent', () => {
  let component: UniversalConfirmationDialogComponent;
  let fixture: ComponentFixture<UniversalConfirmationDialogComponent>;
  let dialog: MatDialog;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        
        BrowserDynamicTestingModule,
        BrowserAnimationsModule,
        
        UniversalConfirmationDialogModule,
        MatDialogModule
      ],
      declarations: [ 
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    dialog = TestBed.get(MatDialog);
    let dialogRef = dialog.open(UniversalConfirmationDialogComponent);

    component = dialogRef.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
