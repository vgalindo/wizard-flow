import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-universal-confirmation-dialog',
  templateUrl: './universal-confirmation-dialog.component.html',
  styleUrls: ['./universal-confirmation-dialog.component.css']
})
export class UniversalConfirmationDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<UniversalConfirmationDialogComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: object
  ) { }

  ngOnInit() {
  }

  onAction( action: string){
    if(action === 'OK'){
      this.dialogRef.close(true);
    }
    if(action === 'CANCEL'){
      this.dialogRef.close(false);
    }
  }



}
