import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AngularMaterialModule } from './angular-material/angular-material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule  } from '@angular/forms';
import 'hammerjs';

import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { UniversalConfirmationDialogComponent } from './universal-confirmation-dialog/universal-confirmation-dialog.component';
import { WizardFlowComponent } from './wizard-flow/wizard-flow.component';
import { NavigatorComponent } from './wizard-flow/navigator/navigator.component';
import { NavigatorService } from './wizard-flow/services/navigator.service';
import { WizardStepsDirective } from './wizard-flow/directives/wizard-steps.directive';
import { StepOneComponent } from './wizard-flow/steps/step-one/step-one.component';
import { StepTwoComponent } from './wizard-flow/steps/step-two/step-two.component';
import { StepIndicatorComponent } from './wizard-flow/step-indicator/step-indicator.component';
import { StepThreeComponent } from './wizard-flow/steps/step-three/step-three.component';
import { StepOneService } from './wizard-flow/steps/step-one/step-one.service';
import { StepTwoService } from './wizard-flow/steps/step-two/step-two.service';
import { StepThreeService } from './wizard-flow/steps/step-three/step-three.service';
import { KabbageComponent } from './kabbage/kabbage.component';
import { KabbageService } from './kabbage/services/kabbage.service';

@NgModule({
  declarations: [
    AppComponent,
    UniversalConfirmationDialogComponent,
    WizardFlowComponent,
    NavigatorComponent,
    WizardStepsDirective,
    StepOneComponent,
    StepTwoComponent,
    StepIndicatorComponent,
    StepThreeComponent,
    KabbageComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FlexLayoutModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    AppRoutingModule,
    /* HttpClientInMemoryWebApiModule.forRoot(
      StepsService
    ) */
  ],
  entryComponents: [
    UniversalConfirmationDialogComponent,
    StepOneComponent,
    StepTwoComponent,
    StepThreeComponent
  ],
  providers: [
    NavigatorService,
    StepOneService,
    StepTwoService,
    StepThreeService,
    KabbageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
