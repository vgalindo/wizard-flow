import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatCardModule, MatInputModule, MatDialogModule, MatSliderModule, MatSelectModule, MatToolbarModule } from '@angular/material';

@NgModule({
  imports: [
    MatButtonModule, MatCardModule, MatInputModule, MatDialogModule, MatSliderModule, MatSelectModule, MatToolbarModule
  ],
  exports: [
    MatButtonModule, MatCardModule, MatInputModule, MatDialogModule, MatSliderModule, MatSelectModule, MatToolbarModule
  ],
  declarations: []
})
export class AngularMaterialModule { }
